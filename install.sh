#!/bin/bash -e

# Copyright (C) 2023 Giovanni Mascellani for CodeWeavers

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

DISK="$1"
CDROM="$2"
SIZE="$3"

rm -f "$DISK"
qemu-img create -f qcow2 "$DISK" "$SIZE"

(cd server && zip ../autounattend/server.zip *)

xorriso -as mkisofs -iso-level 3 -r -V AUTO -o autounattend.iso -J -J -joliet-long autounattend

qemu-system-x86_64 -machine q35 -cpu host -smp 4 -m 8G -bios /usr/share/ovmf/OVMF.fd -enable-kvm -vga qxl -net nic,model=e1000 -net user -usb -usbdevice tablet -device nec-usb-xhci,id=xhci -audio driver=pa,model=hda -hda "$DISK" -cdrom "$CDROM" -drive if=none,id=autounattend,format=raw,file=autounattend.iso,media=cdrom -device usb-storage,bus=xhci.0,drive=autounattend
