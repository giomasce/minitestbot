# Copyright (C) 2023 Giovanni Mascellani for CodeWeavers

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

import tempfile
import os
import base64
import zipfile
import shutil
import traceback
import ctypes

from wsgiref.simple_server import make_server

def app_impl(environ, start_fn):
    request_length = int(environ['CONTENT_LENGTH'])

    with tempfile.TemporaryDirectory() as temp_dir:
        request_filename = os.path.join(temp_dir, "request.zip")

        with open(request_filename, 'wb') as request_file:
            while request_length > 0:
                read_length = min(request_length, 4096)  # Must be a multiple of 4 for base64
                buf = environ['wsgi.input'].read(read_length)
                decoded_buf = base64.b64decode(buf)
                request_file.write(decoded_buf)
                request_length -= len(buf)

        request_dirname = os.path.join(temp_dir, "request")
        with open(request_filename, "rb") as request_file, zipfile.ZipFile(request_file) as request_zip:
            for member in request_zip.infolist():
                target_path = os.path.join(request_dirname, member.filename)
                if target_path.endswith('/'):
                    os.makedirs(target_path, exist_ok=True)
                else:
                    os.makedirs(os.path.dirname(target_path), exist_ok=True)
                    with open(target_path, 'wb') as target_file, request_zip.open(member) as zip_file:
                        shutil.copyfileobj(zip_file, target_file)

        payload_filename = os.path.join(request_dirname, "payload.py")
        with open(payload_filename, "r") as payload_file:
            payload = payload_file.read()
        exec_globals = {}
        exec_locals = {}
        exec(payload, exec_globals, exec_locals)
        for x in exec_locals['execute'](request_dirname):
            yield x

def app(environ, start_fn):
    try:
        generator = app_impl(environ, start_fn)
    except:
        start_fn('500 Internal Server Error', [('Content-Type', 'text/plain')])
        yield traceback.format_exc().encode('utf-8')
    else:
        start_fn('200 OK', [('Content-Type', 'text/plain')])
        for x in generator:
            yield x

def main():
    # Prevent the system from sleeping
    ctypes.windll.kernel32.SetThreadExecutionState(0x80000002)
    with make_server('', 8000, app) as httpd:
        httpd.serve_forever()

if __name__ == '__main__':
    main()
