# MiniTestBot

This is a tool similar in spirit to the [Wine
TestBot](https://testbot.winehq.org/), but meant for being installed
on your computer rather than in the cloud.

The Wine TestBot is a tool to easily test Wine conformance tests on
Windows systems: you submit an Windows executable to the cloud
service, which spawns a QEMU virtual machine, runs your program and
returns you the output. Unfortunately, though, there is a fixed pool
of virtual machines, so if many users submit programs at the same time
they might wait some time before having their results. Also, you are
constrained by the timeout enforced by the service and you cannot
directly access the virtual machine if for some reason you need to.

MiniTestBot does roughly the same thing, but the virtual machine runs
on your hardware, so you have exclusive access to it and can tinker
with it how you please.

MiniTestBot is very experimental and immature for the moment. Expect
interfaces to change wildly.

## How does it work

MiniTestBot is essentially a script (`install.sh`) that (mostly)
automatically sets up a QEMU image with a customized installation of
Windows 10, a server for accepting jobs running inside the virtual
machine (`server.py`) and a client to send jobs to the server
(`minitestbot.py`).

The Windows installation is automatized using an ["answer
file"](https://learn.microsoft.com/en-us/windows-hardware/manufacture/desktop/update-windows-settings-and-scripts-create-your-own-answer-file-sxs?view=windows-10)
(`autounattend.xml`), which at the end spawns a PowerShell script
taking care of some image setup (`install.ps1`).

## Legal considerations

Everything in this repository is free sofware (licensed under the
GPL-3+ license), so you're free to use it however you want. However,
this tool is designed to help you running proprietary software (the
Windows operating system and other software bundled with it), which
can come with a number of requirements and obligations. Whether this
is legal for you is something that depends on your jurisdiction and
other circumstances that I don't know. Also, I am not a lawyer, so I
cannot help you in any way with this matter.

Before running the code in this repository, please review it and check
that you're comfortable with what it is doing. If you're not sure,
consult with a specialized lawyer. I am not reponsible of what happens
when you run this code, you are!

## Security considerations

The virtual machines created by MiniTestBot are intentionally stripped
of a number of security features, to make testing easier. Do not
expose them to unsecure networks and do not use them for production
services. They are only meant to be used for testing.

Notice that by default they listen on port 8000 on the local host
only, but that still means that other users on the same computer can
connect to them and execute random code on them. The connection
between the MiniTestBot client and the virtual machine is not
encrypted or authenticated in any form.

## Tutorial

1. Review the "Legal considerations" paragraph. Ensure that you
   understand what these instructions do and that it is legal for you
   to do that.

2. [Download a Windows 10 installation
   ISO](https://www.microsoft.com/en-US/software-download/windows10). You'll
   probably want the US English 64 bit edition.

3. Run the `install.sh` script:

    ```
    ./install.sh disk.qcow2 Windows10.iso 100G
    ```

   Where `disk.qcow2` is the disk image you're about to create (if
   that file exists already, it will be overwritten!), `Windows10.iso`
   is the ISO you just downloaded and `100G` is the virtual size of
   the created disk.

4. Immediately after startup, a message will appear saying that you
   need to press any button to enter the Windows 10 installer. Press
   any button! After this step the installation should proceed
   automatically, you don't have to interact with it anymore until
   it's finished, except for a point in which you have to confirm you
   want to install a driver (I don't know yet how to automate that).

   The system will reboot a few times, so the "press a button to enter
   the installer" message will appear again. You don't have to press a
   button again, because at that point you have to use the installed
   system.

5. Once the installation process is done, the virtual machine will
   shutdown. Launch it again with `./launch.sh disk.qcow2` instead of
   `install.sh`.

6. Using either a textual or the graphical shell, start
   `c:\minitestbot\server.py` inside the virtual machine. Now the
   machine is ready to accept jobs, listening via HTTP on the port
   8000.

7. (Optional) Access QEMU's monitor shell (either using the menu or
   with Ctrl-Alt-2) and give the command `savevm server`. This will
   create a snapshot of the current state of the virtual machine: from
   now on you can launch with `./launch.sh -loadvm disk.qcow2` and
   have the machine immediately ready.

8. Use the client to run a Wine test inside the virtual machine:

    ```
    ./client/minitestbot.py http://localhost:8000/ ntdll_test.exe virtual
    ```
