#!/usr/bin/env python3

# Copyright (C) 2023 Giovanni Mascellani for CodeWeavers

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

import tempfile
import os
import sys
import requests
import shutil
import base64
import zipfile
import json
import argparse

def main():
    parser = argparse.ArgumentParser(description="Send a job to the MiniTestBot")
    parser.add_argument('target_url', type=str, help="Target URL")
    parser.add_argument('executable_filename', type=str, help="Executable filename")
    parser.add_argument('arguments', type=str, nargs='*', help="Arguments")
    parser.add_argument('-a', '--attach', type=str, action='append', help="Attach file", default=[])
    parser.add_argument('-E', '--environment-variable', type=str, action='append', help='Define an environment variable in the form VARIABLE_NAME=value', default=[])
    parser.add_argument('-S', '--save-directory', type=str, help="Save execution environment to directory")
    args = parser.parse_args()

    env_vars = {}
    for env_var in args.environment_variable:
        split = env_var.split('=', 1)
        if len(split) == 2:
            env_vars[split[0]] = split[1]
        else:
            env_vars[split[0]] = ''

    executable_basename = os.path.basename(args.executable_filename)

    with tempfile.TemporaryDirectory() as temp_dir:
        request_filename = os.path.join(temp_dir, "request.zip")

        payload_args = {
            'executable': executable_basename,
            'arguments': args.arguments,
            'timeout': 60,
            'save_directory': args.save_directory,
            'env_vars': env_vars,
        }

        with zipfile.ZipFile(request_filename, 'w', compression=zipfile.ZIP_DEFLATED) as request_zip:
            with request_zip.open('payload.py', 'w') as zip_file:
                with open(os.path.join(os.path.dirname(__file__), 'payload.py'), 'rb') as payload_file:
                    shutil.copyfileobj(payload_file, zip_file)

            with request_zip.open('args', 'w') as zip_file:
                zip_file.write(json.dumps(payload_args).encode('utf-8'))

            with request_zip.open(executable_basename, 'w') as zip_file:
                with open(args.executable_filename, 'rb') as executable_file:
                    shutil.copyfileobj(executable_file, zip_file)

            for attachment_filename in args.attach:
                attachment_basename = os.path.basename(attachment_filename)
                with request_zip.open(attachment_basename, 'w') as zip_file:
                    with open(attachment_filename, 'rb') as attachment_file:
                        shutil.copyfileobj(attachment_file, zip_file)

        with open(request_filename, 'rb') as request_file:
            request_data = request_file.read()

        encoded_request_data = base64.b64encode(request_data)

        r = requests.post(args.target_url, data=encoded_request_data)
        r.raise_for_status()
        print(r.text.strip())

if __name__ == '__main__':
    main()
