# Copyright (C) 2023 Giovanni Mascellani for CodeWeavers

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

def execute(path):
    import os
    import subprocess
    import json
    import shutil

    args = json.loads(open(os.path.join(path, 'args')).read())
    try:
        completed = subprocess.run([os.path.join(path, args['executable'])] + args['arguments'],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT,
                                   timeout=args['timeout'],
                                   env=args['env_vars'] if len(args['env_vars']) != 0 else None,
                                   cwd=path)
    except subprocess.TimeoutExpired as e:
        yield b'Status: timeout\r\n\r\n'
        if e.stdout is not None:
            yield e.stdout
    else:
        yield b'Status: completed\r\nReturn-Code: '
        yield str(completed.returncode).encode('ascii')
        yield b'\r\n\r\n'
        yield completed.stdout

    if args['save_directory'] is not None:
        shutil.rmtree(args['save_directory'], ignore_errors=True)
        shutil.copytree(path, args['save_directory'])
