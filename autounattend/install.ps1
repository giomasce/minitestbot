# Copyright (C) 2023 Giovanni Mascellani for CodeWeavers

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

Start-Transcript -Path "c:\report.txt"
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
Set-NetFirewallProfile -Enabled False
choco install -y python311 git virtio-drivers 7zip
7z x -oc:\minitestbot d:\server.zip
cd c:\
& "C:\Program Files\Git\cmd\git.exe" clone https://github.com/Sycnex/Windows10Debloater.git
cd Windows10Debloater
.\Windows10SysPrepDebloater.ps1 -Sysprep -Debloat -Privacy
Stop-Transcript
